import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class CustomDocument extends Document {
  static async getInitialProps(ctx) {
    return {...(await Document.getInitialProps(ctx))};
  }

  render() {
    return (
      <Html className="">
        <Head />

        <body className="text-white font-light bg-primary min-h-screen overflow-x-hidden overflow-y-scroll">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
