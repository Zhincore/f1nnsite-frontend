export default function uploadsProxy(req, res) {
  if (!("url" in req.query)) return res.status(400).send("Missing parameter 'url'");

  try {
    const url = new URL(req.query.url);

    if (url.host != process.env.NEXT_PUBLIC_STRAPI_DOMAIN) return res.status(400).send("Invalid host");

    return fetch(url.href).then(result => result.body.pipe(res));
  } catch(err) {
    if (err.message.startsWith("Invalid URL")) res.status(400).send("Invalid URL");
    else throw err;
  }
}
