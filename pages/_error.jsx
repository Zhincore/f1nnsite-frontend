import App from "pages/_app";

export default function Error({ statusCode }) {
  return (
    <>
      Error {statusCode}
    </>
  );
}

Error.getInitialProps = async (ctx) => {
  const { res, err } = ctx;
  const pageProps = await App.getInitialProps(ctx);
  const statusCode = (res && res.statusCode) || (err && err.statusCode) || 404;
  console.log(pageProps);
  return { props: {
    ...pageProps,
    statusCode,
    title: `Error ${statusCode}`,
  } };
}
