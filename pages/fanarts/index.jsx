import { fetchAPI } from "lib/api";
import useFanartSorter from "hooks/useFanartSorter";
import useSearchFilter from "hooks/useSearchFilter";
import FanartIcon from "$elements/FanartIcon";
import Section from "$elements/Section";

export default function Fanarts({ fanarts, title }) {
  const [ filteredFanarts, SearchFilter ] = useSearchFilter(fanarts, ["title", "slug", "author.name"]);
  const [ sortedFanarts, FanartSorter ] = useFanartSorter(filteredFanarts);

  return (
    <main>
      <Section small className="bg-primary-lightish"
        innerClassName="flex flex-wrap text-center justify-between space-y-2 md:space-y-0 flex-col md:flex-row"
      >
        <h1 className="text-xl">{ title }</h1>

        {SearchFilter}

        {FanartSorter}
      </Section>

      <section className="p-2">
        <ul className="flex flex-wrap justify-center">
          { sortedFanarts.map(fanart =>
            <li key={ fanart.slug } className="max-w-1/2 p-2">
              <FanartIcon fanart={ fanart } />
            </li>
          ) }
        </ul>
      </section>
    </main>
  );
}

export async function getStaticProps() {
  const fanarts = await fetchAPI("/fanarts");

  return { props: { fanarts, title: "Fanarts of F1nn" }, revalidate: 60 };
}
