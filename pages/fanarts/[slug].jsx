import Link from "next/link";
import { fetchAPI } from "lib/api";
import Section from "$elements/Section";
import Credits from "$elements/Credits";
import Media from "$elements/Media";

export default function Fanart({ fanart }) {
  return (
    <main>
      <Section small width="max-w-screen-2xl" innerClassName="flex flex-wrap">
        <div className="relative md:w-1/2">
          <Media media={fanart.media} alt={fanart.title} width={1024} height={1024} flexible mediaClass="object-contain" />
        </div>

        <div className="flex-grow md:w-1/2 md:pl-4">
          <header className="flex flex-wrap">
            <div className="flex-grow">
              <h1 className="text-4xl">
                { fanart.title }
              </h1>
              <p className="text-gray-200">Uploaded { (new Date(fanart.created_at).toLocaleDateString()) }</p>
            </div>

            <Link href="/fanarts"><a className="p-2 text-lg text-link hover:text-link-light hover:underline">
              See all fanarts
            </a></Link>
          </header>

          <hr className="mt-4 mb-6" />

          { fanart.author &&
            <>
              <Link href={"/fanarts?q="+encodeURIComponent(fanart.author.name)}>
                <a>
                  <h2 className="text-3xl mb-2">
                    <span className="text-lg text-gray-400 block">Author:</span>
                    { fanart.author.name }
                  </h2>
                </a>
              </Link>

              <Credits links={fanart.author.socials} />
            </>
          }
        </div>
      </Section>
    </main>
  );
}

export async function getStaticProps({ params }) {
  const fanart = await fetchAPI("/fanarts/"+params.slug);
  const props = !fanart ? {} : {
    fanart,
    title: fanart.title,
    breadcrumbs: [["Fanarts of F1nn", "/fanarts"]],
  };

  return { props, notFound: !fanart, revalidate: 60 };
}

export async function getStaticPaths() {
  const fanarts = await fetchAPI("/fanarts");

  return {
    paths: fanarts.map(a => ({ params: { slug: a.slug } })),
    fallback: true,
  };
}
