import { fetchAPI } from "lib/api";
import usePostSorter from "hooks/usePostSorter";
import useSearchFilter from "hooks/useSearchFilter";
import MediaGallery from "$parts/MediaGallery";
import Section from "$elements/Section";

export default function Photos({ photos, title }) {
  const [ filteredPhotos, SearchFilter ] = useSearchFilter(photos, ["alternativeText", "outfit.name", "outfit.slug", "outfit.keywords"]);
  const [ sortedPhotos, PostSorter ] = usePostSorter(filteredPhotos);

  return (
    <main>
      <Section small className="bg-primary-lightish"
        innerClassName="flex flex-wrap text-center justify-between space-y-2 md:space-y-0 flex-col md:flex-row"
      >
        <h1 className="text-xl">{ title }</h1>

        {SearchFilter}

        {PostSorter}
      </Section>

      <section className="p-2">
        <MediaGallery gallery={sortedPhotos} />
      </section>
    </main>
  );
}

export async function getStaticProps() {
  const outfits = await fetchAPI("/outfits");
  const posts = [];

  // TODO: This is pretty much a DDOS... fix this pls
  for (const outfitPreview of outfits) {
    posts.push(fetchAPI("/outfits/"+outfitPreview.slug).then(outfit =>
      outfit.posts.map(post => ({
        ...post,
        alternativeText: post.alternativeText || `F1nn5ter's ${outfit.name} outfit`,
        outfit: { name: outfit.name, slug: outfit.slug, keywords: outfit.keywords },
      }))
    ));
  }
  const photos = (await Promise.all(posts)).flat();

  return { props: {
    photos,
    title: "All photos",
    description: `Our whole collection of F1NN5TER in one place! The gallery currently contains over ${photos.length} photos and videos!`,
  }, revalidate: 36000 };
}
