import App from "next/app";
import Head from "next/head";
import { createContext } from "react";
// import { AnimateSharedLayout, AnimatePresence } from "framer-motion";
import * as fa from '@fortawesome/fontawesome-svg-core';
import {
  faGlobe, faEllipsisH, faSortAmountDown, faSortAmountUp, faSearch, faLink, faCircleNotch, faTimes, faAngleLeft, faAngleRight, faInfoCircle, faDownload, faAngleDoubleLeft, faAngleDoubleRight, faAngleDoubleDown
} from '@fortawesome/free-solid-svg-icons';
import {
  faTwitter, faFacebook, faYoutube, faInstagram, faTwitch, faReddit
} from '@fortawesome/free-brands-svg-icons';
import { fetchAPI, getMedia } from "lib/api";
import compileBreadcrumbs from "lib/breadcrumbs";
import SEO from "$parts/SEO";
import Header from "$parts/Header";
import Footer from "$parts/Footer";
import Breadcrumbs from "$parts/Header/Breadcrumbs";

import "styles/global.scss";

fa.config.autoAddCss = false;
fa.config.keepOriginalSource = false;
fa.config.observeMutations = false;
fa.library.add(faGlobe, faEllipsisH, faSortAmountDown, faSortAmountUp, faSearch, faLink, faCircleNotch, faTimes, faAngleLeft, faAngleRight, faInfoCircle, faDownload, faAngleDoubleLeft, faAngleDoubleRight, faAngleDoubleDown);
fa.library.add(faTwitter, faFacebook, faYoutube, faInstagram, faTwitch, faReddit);

export const GlobalContext = createContext({});

export default class CustomApp extends App {
  static async getInitialProps() {
    const global = await fetchAPI("/global");

    return { pageProps: { site: global } };
  }

  render() {
    const { Component, pageProps, router } = this.props;
    const site = pageProps.site = pageProps.site || { inFallback: true };
    site.baseURL = (
      (process.env.NEXT_PUBLIC_PROTO || "https") + "://" +
      process.env.NEXT_PUBLIC_DOMAIN
    );

    const breadcrumbs = compileBreadcrumbs(pageProps, router.asPath);

    // Next didn't give us basic data, have to return SEO unfriendly page
    if (site.inFallback) return "Loading...";

    return (
      <>
        <Head>
          <meta key="theme-color" name="theme-color" content="#1166bb" />

          { site.favicon &&
            <link key="favicon" rel="shortcut icon" href={ getMedia(site.favicon) } />
          }
          <style key="fontAwesomeCss" dangerouslySetInnerHTML={{ __html: fa.dom.css().replace(/(?<![a-z0-9])[ \n]+/gi, "") }} />
        </Head>
        <SEO {...pageProps} router={ router } breadcrumbs={breadcrumbs} />

        <div className="min-h-screen flex flex-col">{/* i'd put these classes on #__next but it causes Cumulative Layout Shift */}
          <GlobalContext.Provider value={ site }>
            <Header breadcrumbs={ breadcrumbs } site={site} />

            { !!breadcrumbs.length &&
              <Breadcrumbs breadcrumbs={ breadcrumbs } className="md:hidden" />
            }

            <Component key={ router.asPath } {...pageProps} />

            <Footer site={ site } />
          </GlobalContext.Provider>
        </div>
      </>
    );
  }
}
