import { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Markdown from "react-markdown";
import { motion } from "framer-motion";
import { fetchAPI } from "lib/api";
import mdRenderers from "components/mdRenderers";
import Media from "$elements/Media";
import Logo from "$elements/Logo";
import OutfitIcon from "$elements/OutfitIcon";
import FanartIcon from "$elements/FanartIcon";
import Section from "$elements/Section";
import creditUtils from "lib/creditUtils";

import tailwindConf from "tailwind.config";

const BlockGradient = dynamic(() => import("$parts/BlockGradient"));
const ChangingCitation = dynamic(() => import("$parts/ChangingCitation"));

export default function Index({ site, homepage, about, featuredOutfits, featuredFanarts }) {
  const [ isBrowser, setIsBrowser ] = useState(false);

  useEffect(() => {
    setIsBrowser(process.browser); // two-pass rendering
  }, []);

  return (
    <main>
      <header className="relative">
        <div className="relative z-10 min-h-screen flex flex-col justify-center">
          <div className="flex justify-center md:justify-start flex-wrap mb-auto md:mt-auto pb-8 pt-24 px-2 md:px-16 w-full">
            <div className="relative leading-0 max-w-1/3 ml-4">
              <Media hidePlaceholder={true} width={260} height={512} priority={true} alt="Th1cc5ter" src="/f1cc5ter.png" flexible />
              <a href="https://twitter.com/Cynikoarts" target="_blank" rel="noreferrer"
                className="absolute bottom-0 left-0 transform -rotate-90 -translate-x-2 -translate-y-1 origin-bottom-left"
              >
                <FontAwesomeIcon icon={["fab", "twitter"]} /> @Cynikoarts
              </a>
            </div>

            <div className="flex flex-col md:ml-16 max-w-full text-center md:text-left">
              <div className="my-auto">
                <h1>
                  <span className="hidden">{ site.siteName }</span>
                  <Logo className="fill-current h-16 lg:h-24 2xl:h-32 w-auto max-w-full" />
                </h1>

                <p className="text-3xl xl:text-4xl 2xl:text-5xl font-medium md:mt-4">
                  { site.siteSlogan }
                </p>
                <p className="text-3xl md:mt-4 hidden md:block">
                  { site.description }
                </p>
              </div>
            </div>
          </div>

          <div className="flex flex-wrap items-center justify-center px-2">
            { isBrowser &&
              <Link href="/about#quotes"><a className="relative block md:px-2 my-2 text-center md:text-left text-md 2xl:text-xl group flex-grow">
                <ChangingCitation citations={ homepage.citations.map(v => v.content) } />
                {" "}
                <span className="absolute left-4 top-full text-sm text-link group-hover:text-link-light group-hover:underline leading-1/2">
                  Click to see all quotes
                </span>
              </a></Link>
            }

            <ul className="flex flex-wrap items-center md:ml-auto">
              { site.subjectsLinks[0].links.slice(0, 4).map((link, i) =>
                <li key={link.type+i} title={ link.type +": "+ creditUtils.getName(link) }>
                  <a href={ creditUtils.getLink(link) } target="_blank" rel="noreferrer" className="text-4xl block p-2 mx-2 first:ml-0">
                    <FontAwesomeIcon icon={ creditUtils.getIcon(link) } />
                  </a>
                </li>
              ) }
              <li title="more links">
                <a href="#footer" className="text-4xl block p-2 ml-2">
                  <FontAwesomeIcon icon={ ["fas", "ellipsis-h"] } />
                </a>
              </li>
            </ul>
          </div>
        </div>

        <div className="absolute bottom-0 left-0 w-full">
          { isBrowser &&
            <BlockGradient size={16} maxHeight={4} fill={tailwindConf.theme.extend.colors.primary.lightish} />
          }
          <div className="w-full h-16 bg-primary-lightish" />
        </div>

        { homepage.background &&
          <div className="fixed inset-0 -z-1 w-screen h-screen">
            <Media hidePlaceholder={true} layout="fill" alt="" media={homepage.background} quality={100} flexible cover style={{ willChange: "opacity" }} />
          </div>
        }
      </header>

      {/* ### Welcome text ### */}
      <Section className="min-h-1/2-screen bg-primary-lightish text-center">
        <div className="text-xl mb-4">
          <motion.div layoutId="about.greeting">
            <Markdown renderers={ mdRenderers }>{ about.greeting }</Markdown>
          </motion.div>
          <small><Link href="/about"><a className="text-link hover:underline">Read more...</a></Link></small>
        </div>
        &mdash;&mdash;&mdash;
        <Markdown renderers={ mdRenderers }>{ homepage.text }</Markdown>
      </Section>

      {/* ### Featured outfits ### */}
      <Section className="bg-primary-darkish" wide>
        <header className="flex w-full max-w-screen-lg mx-auto flex-wrap mb-2 p-2">
          <h2 className="text-3xl font-medium inline-block mr-auto">
            Featured outfits
          </h2>
          <Link href="/outfits">
            <a className="text-link hover:text-link-light hover:underline text-xl">
              See all
            </a>
          </Link>
        </header>

        <ul className="flex flex-wrap justify-center">
          { featuredOutfits.map(outfit =>
            <li key={ outfit.slug } className="max-w-1/2 p-2">
              <OutfitIcon outfit={ outfit } />
            </li>
          ) }
        </ul>
      </Section>

      {/* ### Featured fanarts ### */}
      <Section className="bg-primary" wide>
        <header className="flex w-full max-w-screen-lg mx-auto flex-wrap mb-2 p-2">
          <h2 className="text-3xl font-medium inline-block mr-auto">
            Featured fanarts
          </h2>
          <Link href="/fanarts">
            <a className="text-link hover:text-link-light hover:underline text-xl">
              See all
            </a>
          </Link>
        </header>

        <ul className="flex flex-wrap justify-center">
          { featuredFanarts.map(fanart =>
            <li key={ fanart.slug } className="max-w-1/2">
              <FanartIcon fanart={ fanart } />
            </li>
          ) }
        </ul>
      </Section>
    </main>
  );
}

export async function getStaticProps() {
  const [homepage, about, featuredOutfits, featuredFanarts] = await Promise.all([
    fetchAPI("/homepage"),
    fetchAPI("/about"),
    fetchAPI("/outfits?featured=true"),
    fetchAPI("/fanarts?featured=true"),
  ]);

  return { props: {
    homepage, about,
    featuredOutfits: featuredOutfits || [],
    featuredFanarts: featuredFanarts || [],
  }, revalidate: 60 };
}
