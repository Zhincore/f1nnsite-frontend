import Media from "$elements/Media";
import Markdown from "react-markdown";
import { motion } from "framer-motion";
import { fetchAPI } from "lib/api";
import mdRenderers from "components/mdRenderers";
import Credits from "$elements/Credits";
import HashAnchor from "$elements/HashAnchor";

export default function About({ homepage, site, about }) {
  const { subject } = site;

  return (
    <main className="text-xl">
      <section className="py-16 px-2 text-center">
        <div className="m-auto max-w-screen-lg w-full">
          <motion.div layoutId="about.greeting" className="mb-8">
            <Markdown renderers={ mdRenderers }>{ about.greeting }</Markdown>
          </motion.div>

          <Markdown renderers={ mdRenderers }>{ about.about }</Markdown>
        </div>
      </section>

      <section className="min-h-1/2-screen py-16 px-2">
        <div className="m-auto max-w-screen-lg w-full">
          <figure className="md:float-left text-center m-2 md:mr-8">
            <figcaption className="text-2xl p-1 pb-2">
              { subject.name }
              { !!(subject.givenName || subject.familyName) &&
                <span className="text-1xl ml-2 text-gray-300">
                  {" "}
                  ({ [subject.givenName, subject.familyName].filter(v=>v).join(" ") })
                </span>
              }
            </figcaption>

            <div className="inline">
              <Media width={256} height={256/subject.image.width*subject.image.height}
                media={ subject.image } alt={ subject.name }
              />
            </div>
          </figure>

          <p className="inline-block mt-2 md:mt-12">
            For more info about F1nn visit the
            {" "}
            <a className="text-link hover:underline" href="https://youtube.fandom.com/wiki/F1NN5TER" target="_blank" rel="noreferrer">
              YouTube wiki on Fandom
            </a>.
          </p>

          <div className="inline-block mt-4">
            <h1 className="text-2xl">F1nn&apos;s links</h1>

            <Credits credits={ site.subjectsLinks } className="md:ml-4" />
          </div>
        </div>
      </section>

      <section className="min-h-1/2-screen py-16 px-2 bg-primary-darkish flex">
        <div className="m-auto max-w-screen-lg w-full">
          <h1 className="relative text-3xl mb-3">
            <HashAnchor id="quotes" />

            F1nn&apos;s quotes
          </h1>
          <ul style={{ listStyleType: "'– '" }}>
            { homepage.citations.map(item =>
              <li key={item.content} className="ml-6 p-1">
                { item.content }
                { item.source &&
                  <small>{" "}<a href={item.source} target="_blank" rel="noreferrer"
                    className="text-link hover:underline cursor-pointer"
                  >
                    [Source]
                  </a></small>
                }
              </li>
            ) }
          </ul>
        </div>
      </section>

      <section className="py-16 px-2 bg-primary-lightish">
        <div className="m-auto max-w-screen-lg w-full">
          <h1 className="relative text-3xl mb-2">
            <HashAnchor id="credits" />

            Credits
          </h1>

          <Credits credits={ site.credits } className="md:ml-4" />
          <Credits credits={ about.extraCredits } className="md:ml-4" />

          <small className="inline-block mt-2">Your links can be here, contact the webmaster if you want to help with this project.</small>
        </div>
      </section>
    </main>
  );
}

export async function getStaticProps() {
  const [ about, homepage ] = await Promise.all([
    fetchAPI("/about"),
    fetchAPI("/homepage"),
  ]);

  const subject = { "@id": "[url]" };

  return { props: {
    about, homepage,
    title: "About",
    pageType: "AboutPage",

    mainEntity: [
      subject,
      { "@type": "ItemList",
        url: process.env.NEXT_PUBLIC_BASE_URL + "/about",
        name: "F1NN5TER's quotes",
        itemListOrder: "Unordered",
        itemListElement: homepage.citations.map((item, i) => ({ "@type": "ListItem",
          item: { "@type": "Quotation",
            position: i+1,
            text: item.content,
            creator: subject,
          },
        })),
      },
    ],

    seoReplace: [ { obj: subject, prop: "@id", value: "subject.url" } ],
  }, revalidate: 60 };
}
