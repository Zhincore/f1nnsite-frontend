import { Fragment } from "react";
import Markdown from "react-markdown";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fetchAPI } from "lib/api";
import mdRenderers from "components/mdRenderers";
import HashAnchor from "$elements/HashAnchor";

export default function About({ faq, title }) {
  return (
    <main>
      <header className="w-full max-w-screen-lg mx-auto text-center px-2 py-4">
        <h1 className="text-4xl">{ title }</h1>

        <Markdown renderers={ mdRenderers } allowDangerousHtml={true}>{ faq.description }</Markdown>
      </header>


      <dl className="text-left mt-8 text-justify">
        { faq.faq.map(({ question, answer, link }, i) =>
          <Fragment key={ i+question }>
            <dt className="text-2xl md:text-3xl px-2 py-4 bg-primary-lightish mt-4 text-center relative">
              <HashAnchor id={ simplifyStr(question) } />

              <span className="block mx-auto w-full max-w-screen-lg text-left">
                <span className="text-gray-400">Q:{" "}</span>
                { question }

                <a href={"#"+simplifyStr(question)} className="ml-4 text-link hover:text-link-light">
                  <FontAwesomeIcon icon={["fas", "link"]} />
                </a>
              </span>
            </dt>
            <dd className="text-lg md:text-xl px-2 py-4 bg-primary-darkish mb-4 text-center">
              <span className="block mx-auto w-full max-w-screen-lg text-left">
                <span className="text-gray-400">A:{" "}</span>
                { answer }{" "}

                { link &&
                  <a href={link} target="_blank" rel="noreferrer" className="text-link hover:text-link-light hover:underline">
                    See here for more.
                  </a>
                }
              </span>
            </dd>
          </Fragment>
        ) }
      </dl>
    </main>
  );
}

function simplifyStr(str) {
  return String(str).replace(/(?:(?![a-z0-9.]).)+/gi, "+").replace(/(^\+)|(\+$)/, "").toLowerCase();
}

export async function getStaticProps() {
  const faq = await fetchAPI("/faq");

  return { props: {
    faq,
    title: "FAQ",

    pageType: "FAQPage",
    mainEntity: faq.faq.map(item => ({
      "@type": "Question",
      name: item.question,
      acceptedAnswer: {
        "@type": "Answer",
        text: item.answer,
      },
    })),
  }, revalidate: 60 };
}
