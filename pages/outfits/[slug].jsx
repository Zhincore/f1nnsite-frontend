import Link from "next/link";
import { fetchAPI } from "lib/api";
import usePostSorter from "hooks/usePostSorter";
import Section from "$elements/Section";
import MediaGallery from "$parts/MediaGallery";

export default function Outfit({ outfit, title }) {
  const [ posts, PostSorter ] = usePostSorter(outfit.posts);

  return (
    <main>
      <Section small className="bg-primary-lightish"
        innerClassName="flex flex-wrap text-center justify-between space-y-2 md:space-y-0 flex-col md:flex-row"
      >
        <Link href="/outfits">
          <a className="text-lg text-link hover:text-link-light hover:underline">
            See all outfits
          </a>
        </Link>

        <h1 className="text-xl">{ title }</h1>

        {PostSorter}
      </Section>

      <section className="p-2">
        <MediaGallery gallery={posts} alt={`F1nn5ter's ${outfit.name} outfit`} />
      </section>
    </main>
  );
}

export async function getStaticProps({ params }) {
  const outfit = await fetchAPI("/outfits/"+params.slug);
  const props = !outfit ? {} : {
    outfit,
    title: outfit.name,
    description: `Photos and videos of F1NN5TER in his ${outfit.name} outfit.`,
    shareImage: outfit.icon,
    breadcrumbs: [["F1nn's Outfits", "/outfits"]],
  };

  return { props, notFound: !outfit, revalidate: 60 };
}

export async function getStaticPaths() {
  const outfits = await fetchAPI("/outfits");

  return {
    paths: outfits.map(a => ({ params: { slug: a.slug } })),
    fallback: false,
  };
}
