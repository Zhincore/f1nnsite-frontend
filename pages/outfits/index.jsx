import Link from "next/link";
import { fetchAPI } from "lib/api";
import useOutfitSorter from "hooks/useOutfitSorter";
import useSearchFilter from "hooks/useSearchFilter";
import OutfitIcon from "$elements/OutfitIcon";
import Section from "$elements/Section";

export default function Outfits({ outfits }) {
  const [ filteredOutfits, SearchFilter ] = useSearchFilter(outfits, ["name", "slug", "keywords"]);
  const [ sortedOutfits, OutfitSorter ] = useOutfitSorter(filteredOutfits);

  return (
    <main>
      <Section small className="bg-primary-lightish"
        innerClassName="flex flex-wrap text-center justify-between space-y-2 md:space-y-0 flex-col md:flex-row"
      >
        <Link href="/photos">
          <a className="text-lg text-link hover:text-link-light hover:underline">
            See all photos
          </a>
        </Link>

        {SearchFilter}

        {OutfitSorter}
      </Section>

      <section className="p-2">
        <ul className="flex flex-wrap justify-center">
          { sortedOutfits.map(outfit =>
            <li key={ outfit.slug } className="max-w-1/2 p-2">
              <OutfitIcon outfit={ outfit } />
            </li>
          ) }
        </ul>
      </section>
    </main>
  );
}

export async function getStaticProps() {
  const outfits = await fetchAPI("/outfits");

  return { props: { outfits, title: "F1nn's Outfits" }, revalidate: 60 };
}
