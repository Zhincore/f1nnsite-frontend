import usePostSorter from "./usePostSorter";

const SORTMODES = {
  default: { sort: ()=>{} },
  title: {
    sort: arr => arr.sort((a,b) => a.title.localeCompare(b.title)),
    ascending: "A-Z",
    descending: "Z-A",
  },
  "upload date": {
    sort: arr => arr.sort((a,b) => new Date(b.updated_at) - new Date(a.updated_at)),
    ascending: "recent first",
    descending: "oldest first",
  },
  "author": {
    sort: arr => arr.sort((a,b) => a.author.name.localeCompare(b.author.name)),
    ascending: "A-Z",
    descending: "Z-A",
  },
};

export default function useFanartSorter(fanarts, defaultSorting="title") {
  return usePostSorter(fanarts, defaultSorting, SORTMODES);
}
