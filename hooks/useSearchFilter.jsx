import { useState, useEffect, useMemo } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function useSearchFilter(list, keys) {
  const urlQuery = global.window && (new URL(window.location.href)).searchParams.get("q") || "";
  const [ query, setQuery ] = useState(urlQuery);
  const [ filtered, setFiltered ] = useState(list);

  useEffect(async () => {
    if (!query) return setFiltered(list);

    const Fuse = (await import('fuse.js')).default
    const fuse = new Fuse(list, { keys });

    setFiltered(fuse.search(query).map(v=>v.item));
  }, [query]);

  const Component = useMemo(() => (
    <div className="bg-primary text-sm relative rounded-lg">
      <FontAwesomeIcon icon={["fas", "search"]} className="absolute top-0 left-0 m-2" />

      <input type="search" value={query}
        className="appearance-none bg-transparent p-1 pl-8 w-full"
        autoComplete="off" onInput={ ({target}) => setQuery(target.value) }
      />
    </div>
  ), [query]);

  return [ filtered, Component ];
}
