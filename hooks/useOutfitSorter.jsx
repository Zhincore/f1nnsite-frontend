import usePostSorter from "./usePostSorter";

const SORTMODES = {
  default: { sort: ()=>{} },
  name: {
    sort: arr => arr.sort((a,b) => a.name.localeCompare(b.name)),
    ascending: "A-Z",
    descending: "Z-A",
  },
  "update date": {
    sort: arr => arr.sort((a,b) => new Date(b.updated_at) - new Date(a.updated_at)),
    ascending: "recent first",
    descending: "oldest first",
  },
  "number of photos": {
    sort: arr => arr.sort((a,b) => a.posts.length - b.posts.length),
    ascending: "less first",
    descending: "more first",
  },
};

export default function useOutfitSorter(outfits, defaultSorting="name") {
  return usePostSorter(outfits, defaultSorting, SORTMODES);
}
