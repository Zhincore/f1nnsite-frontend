import { useState, useMemo, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import InlineSelect from "$elements/InlineSelect";

const SORTMODES = {
  default: { sort: ()=>{}, },
  "upload date": {
    sort: arr => arr.sort((a,b) => new Date(b.updated_at) - new Date(a.updated_at)),
    ascending: "newest first",
    descending: "oldest first",
  },
  resolution: {
    sort: arr => arr.sort((a,b) => a.width*a.height - b.width*b.height),
    ascending: "smallest first",
    descending: "biggest first",
  },
  type: {
    sort: arr => arr.sort((a,b) => a.mime.localeCompare(b.mime)),
    ascending: "normal",
    descending: "reverse",
  },
  random: { sort: shuffleArray },
};
const DIRECTION = {
  ascending: false,
  descending: true,
};
// const GROUPING = {
//   nothing: null,
//   outfit: "outfit",
// };

export default function usePostSorter(posts, defaultSorting="upload date", sortModes=SORTMODES) {
  const [ _posts, setPosts ] = useState(posts);
  const [ sorting, setSorting ] = useState(defaultSorting);
  const [ direction, setDirection ] = useState("ascending");
  // const [ grouping, setGrouping ] = useState("nothing");

  const sortMode = sortModes[sorting];
  const dir = DIRECTION[direction];

  //// Sort
  useEffect(() => {
    const _posts = [...posts];
    setPosts(null);

    (async () => {
      if (sortMode) sortMode.sort(_posts);
      if (dir) _posts.reverse();

      setPosts(_posts);
    })();
  }, [posts, sortMode, dir]);


  const dirs = Object.keys(DIRECTION).map(dir => [dir, sortMode[dir] || dir]);

  //// Render filter
  const Component = useMemo(() => (
    <div>
      <FontAwesomeIcon icon={["fas", "sort-amount-"+(dir ? "down" : "up")]} />
      <InlineSelect options={Object.keys(sortModes)} value={sorting} onChange={({target}) => setSorting(target.value)} autoComplete="off" />{" "}
      <InlineSelect options={dirs} value={direction} onChange={({target}) => setDirection(target.value)} autoComplete="off" />
      {/*
      <InlineSelect options={Object.keys(GROUPING)} value={grouping} onChange={({target}) => setGrouping(target.value)} />*/}
    </div>
  ), [dir, sortModes, sorting, dirs, direction]);

  return [ _posts, Component ];
}

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}
