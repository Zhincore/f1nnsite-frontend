module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}', './hooks/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // 'media' or 'class'
  theme: {
    extend: {
      transitionProperty: {
        padding: "padding",
        box: "width,height",
        "textSize": "font-size",
        transform: "transform,margin",
      },
      scale: {
        '102': '1.025',
      },
      colors: {
        link: {
          light: "#a6f6ff",
          DEFAULT: "#8fdfff",
        },
        primary: {
          dark: "#004494",
          darkish: "#005aa8",
          DEFAULT: "#1166bb",
          lightish: "#2277cc",
          light: "#4499ff",
        },
      },
      zIndex: {
        "-1": -1,
      },
      lineHeight: {
        "0": 0,
        "1/2": 0.5,
      },
      height: {
        "26": "6.5rem",
      },
      maxWidth: {
        "1/2": "50%",
        "1/3": "33%",
      },
      width: {
        "1/4-screen": "25vw",
      },
      minHeight: {
        "1/2-screen": "50vh",
        "3/4-screen": "75vh",
      },
      margin: {
        "-1/4-screen": "-25vw",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
