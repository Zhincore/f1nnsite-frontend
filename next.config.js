// const usePreact = require("./_next-use-preact.js");

module.exports = {
  webpack: config => {
    config.resolve.extensions = ['.wasm', '.mjs', '.js', ".jsx", '.json'];
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"]
    });

    // return usePreact(config, ...args);
    return config;
  },
  images: {
    domains: ["localhost", process.env.NEXT_PUBLIC_STRAPI_DOMAIN || ""],
  },
};
