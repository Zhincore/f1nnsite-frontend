export function getStrapiURL(path="") {
  return `${
    process.env.NEXT_PUBLIC_STRAPI_API_URL || "http://localhost:1337"
  }${path}`;
}

export function getMedia(media) {
  if (!media) return null;
  if (!media.url) {
    console.error("This media has no url", media);
    return null;
  }
  return media.url.startsWith("/") ? getStrapiURL(media.url) : media.url;
}

export function getDownload(media) {
  return "/api/uploadsProxy?url="+encodeURIComponent(getMedia(media));
}

// Helper to make GET requests to Strapi
export async function fetchAPI(path) {
  const requestUrl = getStrapiURL(path);
  const response = await fetch(requestUrl);
  if (response.status != 200) return null;
  try {
    return await response.json();
  } catch (err) { /**/ }
}
