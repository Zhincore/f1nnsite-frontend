export function getCreditName({ type, url, name }) {
  return ({
    twitter: "@",
    instagram: "@",
  }[type] || "") + (name || url);
}

export function getCreditLink({ type, url }) {
  return "https://" + (({
    facebook: "facebook.com/",
    twitter: "twitter.com/",
    youtube: "youtube.com/",
    instagram: "instagram.com/",
    twitch: "twitch.com/",
    reddit: "reddit.com/",
  }[type] || "") + url).replace(/\/+/g,/*/ */ "/");
}

export function getCreditIcon({ type }) {
  return {
    website: ["fas", "globe"],
  }[type] || ["fab", type];
}

const credit = {
  getCreditName,
  getName: getCreditName,
  getCreditLink,
  getLink: getCreditLink,
  getCreditIcon,
  getIcon: getCreditIcon,
};

export default credit;
