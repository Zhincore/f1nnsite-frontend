export default function breadcrumbs(pageProps, pathname) {
  const breadcrumbs = [...(pageProps.breadcrumbs || []), [pageProps.title, pathname]];
  return breadcrumbs.map(breadcrumb => ({
    url: pageProps.site.baseURL+breadcrumb[1],
    name: breadcrumb[0],
  })).filter(item => item.name);
}
