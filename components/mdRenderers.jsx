// https://github.com/remarkjs/react-markdown#appendix-b-node-types
import Link from "next/link";

module.exports = {
  heading: function heading({level, children}) {
    const Tag = "h"+level;
    return (<Tag className={ "text-"+(6-level)+"xl mt-4" }>{ children }</Tag>);
  },
  link: function link({href, children}) {
    const _props = {
      className: "text-link hover:text-link-light hover:underline",
    };

    if (!href.startsWith("http") && !href.startsWith("://")) {
      return (
        <Link href={href}>
          <a {..._props}>{ children }</a>
        </Link>
      );
    }

    return (<a target="_blank" rel="noreferrer" href={href} {..._props}>{ children }</a>);
  },
  paragraph: function paragraph({children}) {
    return (
      <p className="mt-2 first:mt-0">{ children }</p>
    );
  }
};
