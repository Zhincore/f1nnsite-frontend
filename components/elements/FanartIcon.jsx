import Link from "next/link";
import Media from "$elements/Media";

export default function FanartIcon({ fanart }) {
  return (
    <Link href={ "/fanarts/"+fanart.slug }>
      <a style={{ width: 256 }}
        className="block max-w-full h-full cursor-pointer rounded-lg overflow-hidden bg-primary-darkish transition duration-300 hover:shadow-lg"
      >
        <h2 className="block text-center text-lg md:text-2xl p-2 py-4">
          { fanart.title }
        </h2>
        { fanart.media &&
          <Media media={fanart.media} alt={fanart.title} width={256} height={256} flexible cover />
        }
        <p className="py-1 px-2">
          { fanart.author &&
            <><span className="text-gray-400">by:</span> { fanart.author.name }</>
          }
        </p>
      </a>
    </Link>
  );
}
