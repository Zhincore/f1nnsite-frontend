import SVG from "public/f1nnsite-logo.svg";

export default function Logo(props) {
  return (
    <SVG {...props} style={{ stroke: "black", strokeWidth: "1%" }} />
  );
}
