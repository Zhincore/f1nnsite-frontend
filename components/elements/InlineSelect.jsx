export default function InlineSelect({ children, options, ...props }) {
  return (
    <select className="appearance-none inline-block px-1 bg-transparent font-light border-b cursor-pointer" {...props}>
      { options ? options.map((option, i) =>
        Array.isArray(option) ?
          <option key={i+option[0]} value={option[0]}>{ option[1] }</option>
        :
          <option key={i+option} value={option}>{ option }</option>
      ) : children }
    </select>
  );
}
