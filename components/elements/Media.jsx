import { useState } from "react";
import NextImage from "next/image";
import cn from "classnames";
import {getMedia} from "lib/api";

export default function Media({media, src, video, mediaClass, innerClassName, flexible, cover, style, hidePlaceholder, ...props}) {
  const [ loaded, setLoaded ] = useState(false);
  let ref;

  let outerHeight, outerWidth;
  const _props = {...props};
  _props.className = cn(mediaClass, cover && "object-cover");
  delete _props.class;
  delete _props.onLoad;

  if (media) {
    src = getMedia(media);
    video = media.mime.split("/")[0] == "video";
    const ratio = media.width / media.height;

    if (!props.layout || props.layout != "fill") { // Don't apply size calculations for layout='fill'
      let width  = _props.width  || _props.height * ratio;
      let height = _props.height || _props.width / ratio;

      outerWidth  = width;
      outerHeight = height;

      // cover mode
      if (cover) {
        const maxSize = Math.max(_props.width, _props.height);
        const maxScaledSize = Math.max(maxSize * ratio, maxSize / ratio);
        width = maxScaledSize;
        height = maxScaledSize;
      }

      _props.width  = isNaN(width) ?  "auto" : width;
      _props.height = isNaN(height) ? "auto" : height;
    }
  }

  if (!video) {
    delete _props.autoPlay;
    delete _props.loop;
    delete _props.muted;
    delete _props.controls;
  } else {
    delete _props.layout;
    delete _props.unoptimized;
  }

  const checkLoaded = (instant=false) => {
    if (loaded) return;

    const mediaEl = ref.querySelector("img:not([aria-hidden='true']), video");
    if (mediaEl && !mediaEl.src.startsWith("data:image/gif")) {
      if (mediaEl.complete || (mediaEl.readyState && mediaEl.readyState > 1)) {
        if (instant) setLoaded(true);
        else {
          window.requestAnimationFrame(_ =>
            window.requestAnimationFrame(_ => setLoaded(true))
          );
        }

        if (props.onLoad) {
          props.onLoad();
        }
      }
    }
  };

  const onMount = el => {
    if (!el || ref) return;
    ref = el;

    // NextImage styling
    const wrapEl = el.querySelector("div");
    if (wrapEl) {
      wrapEl.classList.add("h-full");
      if (flexible) wrapEl.classList.add("w-full");
    }

    // Callback for too quickly loaded images
    checkLoaded(true);
  };

  return (
    <div className={cn(props.className,
      "leading-0 inline-block transition-colors duration-500 max-w-full max-h-full",
      !(hidePlaceholder || loaded) && "bg-primary-dark animate-pulse",
    )}>
      <div style={{ width: !flexible && outerWidth, height: !flexible && outerHeight, ...style }} ref={onMount}
        className={cn(innerClassName,
          "block transition-opacity duration-500 overflow-hidden max-w-full max-h-full",
          !loaded && "opacity-0",
          flexible && "h-full",
        )}
      >
        { video ?
          <video {..._props} src={src} onLoadedData={_ => checkLoaded()} />
        :
          <NextImage {..._props} src={src} onLoad={_ => checkLoaded()} />
        }
      </div>

      { !video &&
        <noscript>
          <img src={src} alt={_props.alt} className="hidden" decoding="async" />
        </noscript>
      }
    </div>
  );
}
