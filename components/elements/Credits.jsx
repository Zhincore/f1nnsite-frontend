import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import creditUtils from "lib/creditUtils";


export default function Credits({ credits, links, className }) {
  function Links({links}) {
    return (
      links.map(link =>
        <li key={ link.id } className="p-3 inline-block">
          <a href={creditUtils.getLink(link)} target="_blank" rel="noreferrer"
            className="group flex items-center text-link hover:text-link-light"
          >
            <FontAwesomeIcon icon={creditUtils.getIcon(link)} size="2x" fixedWidth />
            <span className="group-hover:underline ml-2">{ " " + creditUtils.getName(link) }</span>
          </a>
        </li>
      )
    );
  }

  if (!credits && links) return (<Links links={ links } />);

  return (
    <ul className={className}>
      { credits.map(credit =>
        <li key={ credit.id } className="mb-2">
          <h2 className="font-medium">{ credit.title }</h2>
          <ul className="text-lg inline">
            <Links links={ credit.links } />
          </ul>
        </li>
      ) }
    </ul>
  );
}
