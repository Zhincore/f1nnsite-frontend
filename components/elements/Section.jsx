import cn from "classnames";

export default function Section(props) {
  return (
    <section className={cn("flex px-2", props.small ? "py-4" : "py-16", props.className)}>
      <div className={cn("m-auto w-full", !props.wide && (props.width || "max-w-screen-lg"), props.innerClassName)}>
        { props.children }
      </div>
    </section>
  );
}
