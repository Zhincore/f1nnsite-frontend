import Link from "next/link";
import Media from "$elements/Media";

export default function OutfitIcon({ outfit }) {
  return (
    <Link href={ "/outfits/"+outfit.slug }>
      <a style={{ width: 256, minHeight: 256 }}
        className="block max-w-full h-full cursor-pointer rounded-lg overflow-hidden bg-primary-lightish transition duration-300 hover:shadow-lg"
      >
        { outfit.icon &&
          <Media media={outfit.icon} alt={outfit.name} width={256} height={256} flexible cover />
        }
        <h2 className="block text-center text-lg md:text-2xl p-2 py-4 h-full">
          { outfit.name }
        </h2>
      </a>
    </Link>
  );
}
