export default function HashAnchor({ id }) {
  return (
    <span id={id} className="absolute -top-20" />
  );
}
