import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import cn from "classnames";
import {getDownload} from "lib/api";
import Media from "$elements/Media";

export const hashPrefix = "media-";

export default function Lightbox({ gallery, alt }) {
  const [ showInfo, setShowInfo] = useState(null);
  const [ lastMedia, setLastMedia ] = useState(null);
  const [ mediaIndex, setMediaIndex ] = useState(null);
  const [ loaded, setLoaded ] = useState(false);

  const media = gallery[mediaIndex];
  const prev = gallery[(gallery.length + mediaIndex-1) % gallery.length].id;
  const next = gallery[(mediaIndex+1) % gallery.length].id;

  const setMedia = index => {
    setLastMedia(media);
    setMediaIndex(index);
  };
  const changeMedia = id => {
    if (id) window.location.hash = hashPrefix+id;
    else {
      // Prevent scrolling up on close
      window.history.replaceState({}, document.title, "#");
      setMedia(null);
    }
  };
  const cachedMedia = media || lastMedia;

  // Reset
  useEffect(() => {
    setLoaded(false);
  }, [media]);

  // Hash url controls
  useEffect(() => {
    const update = () => {
      const id = window.location.hash.substr(1+hashPrefix.length);
      if (!media || media.id != id) setMedia(id ? gallery.findIndex(item => id == item.id) : null);
    };

    update();

    window.addEventListener("hashchange", update, { passive: true });
    return () => window.removeEventListener("hashchange", update);
  }, [media, gallery]);

  // Keyboard controls
  useEffect(() => {
    const keyListener = ({key}) => {
      if (key === "Escape") changeMedia(null);
      if (key === "ArrowRight" || key === " ") changeMedia(next);
      if (key === "ArrowLeft") changeMedia(prev);
      if (key === "ArrowUp") changeMedia(gallery[0].id);
      if (key === "ArrowDown") changeMedia(gallery[gallery.length-1].id);
    };

    document.addEventListener("keyup", keyListener, { passive: true });

    return () => document.removeEventListener("keyup", keyListener);
  }, [mediaIndex]);

  return (
    <div className={cn(
      "fixed inset-0 z-50 flex flex-col md:flex-row transition duration-400 overflow-y-auto md:overflow-hidden",
      media && "bg-primary",
      !media && "pointer-events-none opacity-0",
    )}>
      <div className={cn("relative w-screen h-screen overflow-hidden transition duration-400 flex-shrink-0 md:flex-shrink cursor-auto")}>
        {/* Close button */}
        <div className="absolute left-0 top-0 p-4 cursor-pointer z-10 transition opacity-50 hover:opacity-100"
          onClick={ () => changeMedia(null) }
        >
          <FontAwesomeIcon icon={["fas", "times"]} size="2x" />
        </div>

        {/* Info button */}
        <div className="absolute right-0 top-0 p-4 cursor-pointer z-10 transition opacity-50 hover:opacity-100 hidden md:block"
          onClick={ () => setShowInfo(!showInfo) }
        >
          <FontAwesomeIcon icon={["fas", "angle-double-"+(showInfo ? "right" : "left")]} size="2x" />
          {" "}
          <FontAwesomeIcon icon={["fas", "info-circle"]} size="2x" />
        </div>

        {/* Mobile info hint */}
        <div className="absolute right-0 bottom-0 p-4 cursor-pointer z-10 transition opacity-50 md:hidden"
          onClick={ () => setShowInfo(!showInfo) }
        >
          <FontAwesomeIcon icon={["fas", "info-circle"]} fixedWidth size="lg" /><br/>
          <FontAwesomeIcon icon={["fas", "angle-double-down"]} fixedWidth size="lg" />
        </div>

        {/* Prev button */}
        <div className="absolute left-0 top-1/2 transform -translate-y-1/2 p-4 cursor-pointer z-10 transition opacity-50 hover:opacity-100"
          onClick={ () => changeMedia(prev) }
        >
          <FontAwesomeIcon icon={["fas", "angle-left"]} size="3x" />
        </div>

        {/* Next button */}
        <div className="absolute right-0 top-1/2 transform -translate-y-1/2 p-4 cursor-pointer z-10 transition opacity-50 hover:opacity-100"
          onClick={ () => changeMedia(next) }
        >
          <FontAwesomeIcon icon={["fas", "angle-right"]} size="3x" />
        </div>

        {/* Loader */}
        { !loaded && media &&
          <div className="absolute inset-0 flex">
            <FontAwesomeIcon icon={["fas", "circle-notch"]} spin size="4x" className="m-auto" />
          </div>
        }
        {/* Media */}
        { cachedMedia && <Media autoPlay muted loop controls className="w-full h-full"
          key={"lighbox-"+cachedMedia.id} media={cachedMedia} alt={alt} mediaClass="object-contain w-full h-full"
          onLoad={ _ => setLoaded(true) } unoptimized flexible hidePlaceholder
        /> }
      </div>

      { cachedMedia &&
        <div class={cn(
          "md:h-full w-full md:w-1/4-screen md:overflow-hidden break-all p-4 transition-transform duration-400 origin-right md:transform-gpu",
          "flex flex-col",
          !media || !showInfo && "md:scale-x-0 md:-mr-1/4-screen"
        )}>
          <h2 className="text-2xl font-bold">{ cachedMedia.name }</h2>
          <div>{ cachedMedia.size } kB { cachedMedia.mime }</div>
          <div>{ cachedMedia.width } x { cachedMedia.height } px</div>

          <div className="mt-auto">
            <a href={ getDownload(cachedMedia) } download={ cachedMedia.name }
              className="px-4 py-3 block rounded bg-link w-full font-bold m-1 text-primary text-center"
            >
              <FontAwesomeIcon icon={["fas", "download"]} className="mr-2" />
              Download
            </a>
          </div>
        </div>
      }
    </div>
  );
}
