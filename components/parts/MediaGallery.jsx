import { useState, useEffect } from "react";
import cn from "classnames";
import Lightbox, {hashPrefix} from "$parts/Lightbox";
import Media from "$elements/Media";
import HashAnchor from "$elements/HashAnchor";

const chunking = 16;
const precision = 1/5;
const breakpoints = {
  280: 2,
  460: 3,
  640: 4,
  920: 5,
  1280: 6,
  1920: 7,
  2440: 8,
};

export default function MediaGallery({ gallery, alt }) {
  const [ screenWidth, setScreenWidth ] = useState(1);

  useEffect(() => {
    const listener = () => {
      setScreenWidth(window.innerWidth);
    };
    listener();

    window.addEventListener("resize", listener, {passive: true});

    return () => {
      window.removeEventListener("resize", listener);
    };
  }, []);

  // Find the correct breakpoint
  let nColumns = 1;
  for (const breakpoint of Object.entries(breakpoints)) {
    if (breakpoint[0] > screenWidth) break;
    nColumns = breakpoint[1];
  }
  const columnWidth = screenWidth / nColumns;

  // Generate the columns
  const columns = Array(nColumns).fill(0).map(_ => ({ items: [], height: 0 }));
  const indexPool = []; // index pool which makes items prefer the center over the left corner
  let indexPoolDirection = false;
  for (let i = 0; i < columns.length; i++) {
    if (indexPoolDirection) indexPool.push(i);
    else indexPool.unshift(i);
    indexPoolDirection = !indexPoolDirection;
  }

  // Assign items to columns
  for (const item of gallery) {
    // find the shortest column
    let shortest;
    for (const index of indexPool) {
      const column = columns[index];
      if (!shortest || column.height < shortest.height) shortest = column;
    }
    // and add the item there
    shortest.items.push(item);
    const height = Math.ceil(item.height/item.width * columnWidth * precision) / precision;
    shortest.height += isNaN(height) ? 0 : height;
  }

  return (
    <>
      <div className={cn("flex justify-center leading-0 items-start space-x-2 select-none", !screenWidth && "hidden")}>
        { columns.map((column, i) =>
          <div key={nColumns+":"+i} className="flex flex-col justify-start space-y-2">
            { column.items.map(media =>
              <a key={media.id} href={"#"+hashPrefix+media.id} className="cursor-pointer relative">
                <HashAnchor id={hashPrefix+media.id} />
                <Media media={media} alt={alt || media.alternativeText} width={columnWidth} flexible />
              </a>
            ) }
          </div>
        ) }
      </div>

      <Lightbox gallery={gallery} alt={alt} />
    </>
  );
}
