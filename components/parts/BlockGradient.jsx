import { useState, useEffect } from "react";

export default function BlockGradient({ size, maxHeight, fill, stroke, ...props }) {
  if (!global.window) return null;

  const [width, setWidth] = useState(window.innerWidth);

  useEffect(() => {
    const listener = () => {
      setWidth(window.innerWidth);
    };

    window.addEventListener("resize", listener, { passive: true });

    return () => {
      window.removeEventListener("resize", listener);
    };
  }, []);


  const height = size * maxHeight;
  const blocks = [];
  let y = maxHeight / 2;

  for (let x = 0; x < width; x += size) {
    for (let _y = 1; _y <= y; _y++) {
      blocks.push(
        <rect key={x+":"+_y} x={x-1} y={height-size*_y-1} width={size+2} height={size+2} />
      );
    }
    y = Math.min(maxHeight, Math.max(0, y + (Math.random() * 2 - 1)));
  }

  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox={[0, 0, width, height].join(" ")} {...props}>
      <g fill={fill} stroke={stroke}>
        { blocks }
      </g>
    </svg>
  );
}
