import Credits from "$elements/Credits";
import HashAnchor from "$elements/HashAnchor";

export default function Footer({ site }) {
  return (
    <div className="relative mt-auto">
      <HashAnchor id="footer" />

      <footer className="bg-primary-dark flex flex-wrap justify-around py-4">
        <section className="p-2 my-4">
          <div className="text-xl">
            { site.siteName } is developed and designed by <a href="https://zhincore.eu/" className="text-link hover:underline" target="_blank" rel="noreferrer">Zhincore</a>.
          </div>
          <div>
            Images on this site belong to their respective owners.
          </div>

          <Credits credits={ site.credits } className="mt-2" />
        </section>

        <section className="p-2 my-4">
          <h1 className="text-2xl">F1nn&apos;s links</h1>

          <Credits credits={ site.subjectsLinks } />
        </section>
      </footer>
    </div>
  )
}
