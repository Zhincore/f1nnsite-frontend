import { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from 'next/router';
import cn from "classnames";
import Logo from "$elements/Logo";
import MobileHamburger from "./MobileHamburger";
import Breadcrumbs from "./Breadcrumbs";

export default function Header({ breadcrumbs, site }) {
  const { asPath } = useRouter();
  const [ showMobileMenu, setMobileMenu ] = useState(false);
  const [ scrolled, setScrolled ] = useState(asPath == "/");
  const { siteName, menu } = site;

  useEffect(() => {
    const listener = () => {
      setScrolled(window.scrollY > 1);
    };
    listener();

    document.addEventListener("scroll", listener, { passive: true });

    return () => {
      document.removeEventListener("scroll", listener);
    };
  }, [ asPath ]);

  return (
    <>
      { asPath != "/" && /* position: fixed compensator */
        <div className="h-16 md:h-26" id="I_am_a_header_placeholder_lol" />
      }

      <header className={cn("z-30 top-0 w-full fixed transition duration-300",
        (scrolled || asPath != "/" || showMobileMenu) && "shadow-md bg-primary"
      )}>
        <div className={cn("relative flex justify-between items-center md:justify-start md:space-x-10 max-w-7xl mx-auto px-4 sm:px-6 py-4 transition-padding duration-300",
          scrolled ? "md:py-2" : "md:py-5",
        )}>
          <div className="flex flex-wrap justify-start items-end lg:w-0 lg:flex-1">
            <Link href="/"><a onClick={ _ => setMobileMenu(false) }>
              <span className="sr-only">{ siteName }</span>
              <Logo className={cn("fill-current transition-box duration-300 h-8 w-auto md:h-10", /*!scrolled && asPath == "/" ? "md:h-12" : "md:h-10"*/)} />
            </a></Link>

            <Breadcrumbs breadcrumbs={ breadcrumbs } className="hidden md:block" />
          </div>

          <MobileHamburger onClick={ _ => setMobileMenu(!showMobileMenu) } />

          <nav className={ cn(
            "transition transform md:scale-y-100 origin-top bg-primary md:bg-transparent md:static absolute top-full w-full inset-x-0 md:w-auto",
            !showMobileMenu && "scale-y-0",
          ) }>
            <ul className={ cn(
              "flex md:space-x-4 md:flex-row flex-col w-fullshadow-md md:shadow-none text-xl w-full transition-textSize duration-300",
              scrolled ? "md:text-xl" : (asPath != "/" ? "md:text-2xl" : "md:text-3xl"),
            ) }>
              { menu.map((item, i) =>
                <li key={ i+item.title }>
                  <Link href={ item.link }>
                    <a onClick={ _ => setMobileMenu(false) } className={ cn(
                      asPath.startsWith(item.link) ? "text-gray-100" : "text-gray-300",
                      "block hover:text-white rounded-full transition duration-200 py-4 px-6 w-full md:hover:bg-gray-900 md:hover:bg-opacity-25",
                    ) }>
                      { item.title }
                    </a>
                  </Link>
                </li>
              ) }
            </ul>
          </nav>
        </div>
      </header>
    </>
  );
}
