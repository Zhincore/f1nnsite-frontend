import { useContext } from "react";
import Link from "next/link";
import cn from "classnames";
import { motion, AnimatePresence } from "framer-motion";
import { GlobalContext } from "pages/_app";
import styles from "./Breadcrumbs.module.scss";

const titleAnim = {
  "0": { x: "100%", opacity: 0 },
  "1": { x: 0, opacity: 1 },
  "2": { x: "100%", opacity: 0, position: "absolute" },
};

export default function Breadcrumbs({ breadcrumbs, className }) {
  const { siteName } = useContext(GlobalContext);
  const lastI = breadcrumbs.length - 1;

  return (
    <ul className={ cn("relative text-xl", styles.breadcrumbs, className) }>
      <AnimatePresence initial={false}>
        <li className="inline">{""}<span className="hidden">{ siteName }</span></li>
        { breadcrumbs.map(({ url, name }, i) =>
          <li key={"title_"+name} className="inline">
            <motion.div className={cn("inline-block", styles.title)} variants={ titleAnim } initial="0" animate="1" exit="2" transition={{ type: "tween" }}>
              { i == lastI ?
                <span className="inline">{ name }</span>
              :
                <Link href={url}><a className="inline text-gray-300 hover:underline">{ name }</a></Link>
              }
            </motion.div>
          </li>
        ) }
      </AnimatePresence>
    </ul>
  );
}
