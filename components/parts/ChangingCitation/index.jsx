import { Component, Fragment } from "react";
import cn from "classnames";
import styles from "./ChangingCitation.module.scss";
import CONFIG, { letterDelay, delayPerLetter, minDelay } from "./config";

export default class ChangingCitation extends Component {
  constructor(props, ...args) {
    super(props, ...args);
    this.state = {
      show: false,
      citation: "",
    };

    this.lastCitation = "";
    this.unusedCitations = [];
    this.timeout = null;
    this.shouldRun = true;
  }

  componentDidMount() {
    this.changeCitation();
  }

  componentWillUnmount() {
    if (this.timeout) clearTimeout(this.timeout);
    this.shouldRun = false;
  }

  changeCitation() {
    if (!this.shouldRun);

    this.lastCitation = this.state.citation;
    const { unusedCitations } = this;

    // Reset citations if needed
    if (!unusedCitations.length) {
      unusedCitations.push(...this.props.citations);
    }

    // Choose random which wasn't previously
    let citation, index;
    do {
      index = Math.floor(Math.random() * unusedCitations.length);
      citation = unusedCitations[index];
    } while (unusedCitations.length > 2 && citation == this.lastCitation);

    // Remove the citation
    unusedCitations.splice(index, 1);

    // Remove previous letters
    this.setState({ show: false });

    // Wait for removing to finish
    this.timeout = setTimeout(() => {
      // Apply new contents
      this.setState({ citation, show: true });

      // Repeat
      this.timeout = setTimeout(() => {
        this.changeCitation();
      }, minDelay + citation.length * delayPerLetter);
    }, this.lastCitation.length * letterDelay + CONFIG.letterAnimDuration);
  }

  render() {
    const { citation, show } = this.state;

    let letterI = 0;

    return (
      <div className={ styles.wrap }
        style={{
          ["--anim-duration"]: CONFIG.letterAnimDuration+"ms",
          width: this.props.width,
        }}
      >
        <span key="_quoteL" className={ cn(styles.quote, { [styles.show]: show }) }>&ldquo;</span>

        { citation.split(" ").map((word, i) =>
          <Fragment key={ citation+i+show }>
            { !!i && " " /* space between words */ }

            {/* <WORD> */}
            <span className={ styles.word }>
              { Array.from(word).map((letter, j) =>
                /* <LETTER> */
                <span key={ citation+i+j+show }
                  className={ cn(styles.letter, { [styles.show]: show }) }
                  style={{ animationDelay: (letterI++ * letterDelay)+"ms" }}
                >
                  { letter }
                </span>
              ) }
            </span>
          </Fragment>
        ) }

        <span key="_quoteR" className={ cn(styles.quote, { [styles.show]: show }) }>&rdquo;</span>
        <span key="_author" className={ cn(styles.author, { [styles.show]: show }) }>
          {" "} &ndash;&nbsp;F1NN5TER
        </span>
      </div>
    );
  }
}
