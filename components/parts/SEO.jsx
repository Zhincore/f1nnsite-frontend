import Head from "next/head";
import { getMedia } from "lib/api";

export default function SEO({router, ...props}) {
  const { isFallback, asPath } = router;
  const { seo, title, breadcrumbs, description, shareImage, site, pageType, mainEntity } = props;
  const { defaultSeo, siteName, siteSlogan, subject, baseURL } = site;
  const seoWithDefaults = {
    ...defaultSeo,
    ...(title ? { metaTitle: title } : {}),
    ...(description ? { metaDescription: description +" "+siteName+"! "+ defaultSeo.metaDescription } : {}),
    ...(shareImage ? { shareImage: shareImage } : {}),
    ...seo,
  };
  const fullSeo = {
    ...seoWithDefaults,
    // Add title suffix
    metaTitle: [...breadcrumbs.map(item => item.name).reverse(), siteName, siteSlogan].filter(v=>v).join(" | "),
    // Get full image URL
    shareImage: getMedia(seoWithDefaults.shareImage),
  };

  if (isFallback) return null;

  const url = baseURL + asPath;
  const subjectId = subject.url;

  // Replace entity
  function replaceEntity(entity) {
    // if entity is not a string, leave it as is
    if (typeof entity !== "string") return entity;

    const address = entity.split(".");
    let result = {};

    switch (address.shift()) {
      case "subject": result = subject; break;
    }

    for (const subAddress of address) {
      result = result[subAddress];
    }

    return result;
  }

  // SeoReplace
  if ("seoReplace" in props) {
    for(const {obj, prop, value} of props.seoReplace) {
      obj[prop] = replaceEntity(value);
    }
  }

  return (
    <Head>
      <title>{fullSeo.metaTitle}</title>
      <meta property="og:title" content={fullSeo.metaTitle} />
      <meta name="twitter:title" content={fullSeo.metaTitle} />

      <meta property="og:url" content={url} />

      {fullSeo.metaDescription && (
        <>
          <meta name="description" content={fullSeo.metaDescription.replace(/\n+/g, " ")} />
          <meta property="og:description" content={fullSeo.metaDescription.replace(/\n+/g, " ")} />
          <meta name="twitter:description" content={fullSeo.metaDescription.replace(/\n+/g, " ")} />
        </>
      )}
      {fullSeo.shareImage && (
        <>
          <meta name="image" content={fullSeo.shareImage} />
          <meta property="og:image" content={fullSeo.shareImage} />
          <meta name="twitter:image" content={fullSeo.shareImage} />
        </>
      )}
      {fullSeo.article && <meta property="og:type" content="article" />}
      <meta name="twitter:card" content="summary_large_image" />

      {/* ### JSON-LD ### */}

      {/* Subject definition */}
      <script type="application/ld+json" dangerouslySetInnerHTML={{ __html:
        JSON.stringify({
          "@context": "https://schema.org",
          "@type": "Person",
          "@id": subjectId,
          url: subject.url,
          name: subject.name,
          additionalName: subject.name,
          givenName: subject.givenName,
          jobTitle: subject.jobTitle,
          image: getMedia(subject.image),
          nationality: subject.nationality,
        })
      }} />

      {/* WebSite definition */}
      { !breadcrumbs.length && /* only on homepage */
        <script type="application/ld+json" dangerouslySetInnerHTML={{ __html:
          JSON.stringify({
            "@context": "https://schema.org",
            "@type": "WebSite",
            "@id": baseURL,
            name: siteName,
            description: defaultSeo.metaDescription,
            url: baseURL,
            ...(fullSeo.shareImage ? { image: fullSeo.shareImage, thumbnailUrl: fullSeo.shareImage } : {}),
            about: { "@id": subjectId },
          })
        }} />
      }

      {/* Organisation definition */}
      <script type="application/ld+json" dangerouslySetInnerHTML={{ __html:
        JSON.stringify({
          "@context": "https://schema.org",
          "@type": "Organization",
          url: baseURL,
          name: siteName,
          slogan: siteSlogan,
          logo: getMedia(site.logo),
          knowsAbout: { "@id": subjectId },
        })
      }} />

      {/* WebPage definition */}
      <script type="application/ld+json" dangerouslySetInnerHTML={{ __html:
        JSON.stringify({
          "@context": "https://schema.org",
          "@type": pageType || "WebPage",
          "@id": url,
          //// WEBPAGE
          url: url,
          name: fullSeo.metaTitle,
          headline: fullSeo.metaTitle,
          description: fullSeo.metaDescription,
          ...(fullSeo.shareImage ? { image: fullSeo.shareImage, thumbnailUrl: fullSeo.shareImage } : {}),

          //// BREADCRUMBS
          ...(breadcrumbs.length ? { breadcrumb: { "@type": "BreadcrumbList",
            name: "Breadcrumbs", // WARNING: String literal
            itemListElement: breadcrumbs.map((item, i) => ({ "@type": "ListItem",
              position: i + 1,
              item: { "@id": item.url, name: item.name },
            })),
          }} : {}),

          //// mainEntity
          ...(mainEntity ? { mainEntity } : {}),
        })
      }} />
    </Head>
  );
}
